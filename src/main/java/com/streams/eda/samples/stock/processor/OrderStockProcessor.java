package com.streams.eda.samples.stock.processor;

import com.streams.samples.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@EnableBinding(OrderStockProcessor.StreamProcessor.class)
public class OrderStockProcessor {

    @StreamListener
    @SendTo({"orderItemStockReserved", "orderStockReserveCompleted", "orderWorkflowStepCompleted"})
    public KStream<?, ?>[] process(@Input("orderPlaced") KStream<OrderKey, OrderPlaced> orderPlacedStream) {
        orderPlacedStream.peek((k, v) -> {
            log.info("Received an order with ID {}", k);
        });

        KStream<OrderKey, OrderItemStockReserved> orderItemReservedStream = orderPlacedStream.flatMap(this::reserveStockForItems);
        //Publishing OrderStockReserveCompleted is meant to be done only when stock for all order items are reserved(may be items are not available for some time).
        //For sake of simplicity, I will publish them now
        KStream<OrderKey, OrderStockReserveCompleted> orderStockReserveCompletedStream = orderPlacedStream.mapValues(this::completeOrderStockReservation);

        KStream<OrderKey, OrderWorkflowStepCompleted> workflowStepCompletedStream = orderPlacedStream.mapValues(this::toStepCompleted);

        return new KStream[] {orderItemReservedStream, orderStockReserveCompletedStream, workflowStepCompletedStream};
    }

    private OrderStockReserveCompleted completeOrderStockReservation(OrderPlaced orderPlaced) {
        return new OrderStockReserveCompleted(orderPlaced.getOrderItems().size(), currentTime());
    }

    private OrderWorkflowStepCompleted toStepCompleted(OrderKey orderKey, OrderPlaced orderPlaced) {
        return new OrderWorkflowStepCompleted("order-stock-reserved", "order-placed", currentTime());
    }

    private List<KeyValue<OrderKey, OrderItemStockReserved>> reserveStockForItems(OrderKey orderKey, OrderPlaced orderPlaced) {
        int totalOrderItems = orderPlaced.getOrderItems().size();

        return orderPlaced.getOrderItems().stream()
                .map(i -> new OrderItem(i.getId(), i.getQuantity()))
                .map(i -> new OrderItemStockReserved(i, totalOrderItems, currentTime()))
                .map(i -> KeyValue.pair(orderKey, i))
                .collect(Collectors.toList());

    }

    private long currentTime() {
        return Instant.now().toEpochMilli();
    }

    public interface StreamProcessor {
        @Input("orderPlaced")
        KStream<?, ?> orderPlaced();

        @Output("orderItemStockReserved")
        KStream<?, ?> orderItemStockReserved();

        @Output("orderStockReserveCompleted")
        KStream<?, ?> orderStockReserveCompleted();

        @Output("orderWorkflowStepCompleted")
        KStream<?, ?> orderWorkflowStepCompleted();
    }
}
