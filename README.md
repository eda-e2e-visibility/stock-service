# Order stock service

Manage the stock reservation of order items. The following diagram shows the flow of events going to and from this service.

<p align="center">
  <img  width="700" src="docs/order-stock-service.png?raw=true" title="Order stock service">
</p>

## How it works

It consumes `order-placed` to reserve the stock of each order item. This service is publishing the following events

* `order-item-stock-reserved`: for each item in the order, the service is publishing an event of that type.
* `order-stock-reserve-completed`: when stock of all order items are reserved, the service is publishing one event of that type.

## Workflow tracking events

Because this service is part of order business process(aka workflow), it publishes a `order-workflow-step-completed` along side the business events.
For simplicity, I chose to publish this workflow tracking event only when order stock reservation is completed. The following code snippet shows how this works.

```java
KStream<OrderKey, OrderStockReserveCompleted> orderStockReserveCompletedStream = orderPlacedStream.mapValues(this::completeOrderStockReservation);
KStream<OrderKey, OrderWorkflowStepCompleted> workflowStepCompletedStream = orderPlacedStream.mapValues(this::toStepCompleted);
```

## Future work

* Support SAGA transaction: given that order might be expired at any point in the workflow(for example the payment-service fails to invoice this order), we need to
support doing a compensation for the order stock reservation if it was already reserved. Or ignore if it was not reserved yet.
* Skip expired orders: when order is expired we need to make sure that it stops in any workflow step. For example if order is expired just before the stock service start processing the order.
In this case we do not want to reserve stock at all. Moreover, if the stock-service is in the middle of reserving the stock for that order(i.e just before publishing `order-stock-reserve-completed`), we might not publish this event and instead cancel all reserved stock of order items. 